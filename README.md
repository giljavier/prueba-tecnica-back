# Getting Started with API node js

## Available Scripts

In the project directory, you can run:

### `npm install`

### `npm run test`
### `node src/index.js`

this project run with mysql how integration in database

this project this thinking by use the scalable architecture 

CREATE TABLE `inventario`.`usuarios`(
`id` int NOT NULL AUTO_INCREMENT,
`usuario` varchar(75) NOT NULL, 
`password` varchar(255) NOT NULL,
`rol_id` int(25)  NULL,
PRIMARY KEY(`id`)
ALTER TABLE usuarios ADD constraint FK_rol_id FOREIGN KEY (rol_id) REFERENCES roles(id)
)

CREATE TABLE `inventario`.`roles`(
`id` int NOT NULL AUTO_INCREMENT,
`tipo` varchar(75) NOT NULL, 
PRIMARY KEY(`id`)
)

CREATE TABLE `inventario`.`empresas`(
`nit` varchar(75) NOT NULL,
`nombre` varchar(75) NOT NULL, 
`direccion` varchar(75) NOT NULL,
`telefono` varchar(25) NOT NULL,
`user_id` int(25) NOT NULL, 
PRIMARY KEY(`nit`)
ALTER TABLE empresas ADD constraint FK_user_id FOREIGN KEY (user_id) REFERENCES usuarios(id)
)

CREATE TABLE `inventario`.`inventario`(
`id` int NOT NULL AUTO_INCREMENT,
`articulo_id` int(25) NOT NULL, 
`empresa_id` varchar(25) NOT NULL,
PRIMARY KEY(`id`)
ALTER TABLE inventario ADD constraint FK_articulo_id FOREIGN KEY (articulo_id) REFERENCES articulos(id)
ALTER TABLE inventario ADD constraint FK_empresa_id FOREIGN KEY (empresa_id) REFERENCES empresas(nit)
)

CREATE TABLE `inventario`.`articulos`(
`id` int NOT NULL AUTO_INCREMENT,
`nombre` varchar(25) NOT NULL, 
`talla` varchar(25) NOT NULL,
PRIMARY KEY(`id`)
)
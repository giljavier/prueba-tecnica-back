const { LoginModel } = require("../../dominio/models/login.model");
const { LoginController } = require("../controllers/login.controller");

describe("LoginController", () => {
  describe("signIn", () => {
    it("should return an error message when email or password is null", async () => {
      const req = { body: { email: null, password: null } };
      const res = { status: jest.fn().mockReturnThis(), json: jest.fn() };

      await LoginController.sigIn(req, res);

      expect(res.status).toHaveBeenCalledWith(500);
      expect(res.json).toHaveBeenCalledWith({ message: "El correo o la contraseña estan vacia" });
    });

    it("should call LoginModel.get and send its response", async () => {
      const email = "test@test.com";
      const password = "testPassword";
      const user = { id: 1, email, password };
      LoginModel.get = jest.fn().mockResolvedValue(user);

      const req = { body: { email, password } };
      const res = { send: jest.fn() };

      await LoginController.sigIn(req, res);

      expect(LoginModel.get).toHaveBeenCalledWith(email, password);
      expect(res.send).toHaveBeenCalledWith(user);
    });

    it("should return an error response if LoginModel.get throws an error", async () => {
      const error = { message: "Error getting user from database" };
      LoginModel.get = jest.fn().mockRejectedValue(error);

      const req = { body: { email: "test@test.com", password: "testPassword" } };
      const res = { status: jest.fn().mockReturnThis(), json: jest.fn() };

      await LoginController.sigIn(req, res);

      expect(res.status).toHaveBeenCalledWith(500);
      expect(res.json).toHaveBeenCalledWith(error);
    });
  });

  describe("signUp", () => {
    it("should call LoginModel.create and send its response", async () => {
      const email = "test@test.com";
      const password = "testPassword";
      const user = { id: 1, email, password };
      LoginModel.create = jest.fn().mockResolvedValue(user);

      const req = { body: { email, password } };
      const res = { send: jest.fn() };

      await LoginController.singUp(req, res);

      expect(LoginModel.create).toHaveBeenCalledWith({ usuario: email, password: expect.any(String) });
      expect(res.send).toHaveBeenCalledWith(user);
    });

    it("should return an error response if LoginModel.create throws an error", async () => {
      const error = { message: "Error creating user in database" };
      LoginModel.create = jest.fn().mockRejectedValue(error);

      const req = { body: { email: "test@test.com", password: "testPassword" } };
      const res = { status: jest.fn().mockReturnThis(), json: jest.fn() };

      await LoginController.singUp(req, res);

      expect(res.status).toHaveBeenCalledWith(500);
      expect(res.json).toHaveBeenCalledWith(error);
    });
  });
});

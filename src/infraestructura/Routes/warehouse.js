const authMiddleware = require('../../aplicacion/middleware/auth.middleware')
const { Router } = require('express');
const router = Router();
const { WarehouseController } = require("../controllers/warehouse.controller");

router.use(function(req, res, next) {
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, access-token")
    res.header("Access-Control-Allow-Origin", "*")
    next()
});

router.get("/listarInventario",authMiddleware ,WarehouseController.ListWarehouse);
router.post("/crearInventario",authMiddleware, WarehouseController.PostWarehouse);

module.exports = router;
const authMiddleware = require('../../aplicacion/middleware/auth.middleware')
const { Router } = require('express');
const router = Router();
const { TaskController } = require("../controllers/task.controller");

router.use(function(req, res, next) {
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, access-token")
    res.header("Access-Control-Allow-Origin", "*")
    next()
});

router.get("/listarEmpresa",authMiddleware ,TaskController.ListTask);
router.post("/crearEmpresa",authMiddleware, TaskController.PostTask);
router.put("/:id",authMiddleware, TaskController.UpdateTask);

module.exports = router;
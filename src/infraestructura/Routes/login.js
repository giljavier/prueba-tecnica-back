const { Router } = require('express');
const router = Router();
const { LoginController } = require("../controllers/login.controller");

router.use(function(req, res, next) {
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, access-token")
    res.header("Access-Control-Allow-Origin", "*")
    next()
});

router.post('/login', LoginController.sigIn);
router.post('/registrate', LoginController.singUp);

module.exports = router;
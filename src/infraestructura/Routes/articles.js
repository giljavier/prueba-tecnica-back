const authMiddleware = require('../../aplicacion/middleware/auth.middleware')
const { Router } = require('express');
const router = Router();
const { ArticlesController } = require("../controllers/article.controller");

router.use(function(req, res, next) {
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, access-token")
    res.header("Access-Control-Allow-Origin", "*")
    next()
});

router.get("/listarArticulos",authMiddleware ,ArticlesController.ListArticles);
router.post("/crearArticulos",authMiddleware, ArticlesController.PostArticles);
router.put("/:id",authMiddleware, ArticlesController.UpdateArticles);

module.exports = router;
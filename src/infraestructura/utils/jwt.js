const jwt = require('jsonwebtoken');
const privateKey = 'e7e342d3303bd95068f6508ee3796260cfd60b17a87a596556dd8d983af87fe1';

const generateToken = (data)=>{
    const token = jwt.sign({
        exp: Math.floor(Date.now() / 1000) + (60 * 60),
        data: data
      }, privateKey);
    return token;
}

function verifyToken(token) {
    try {
      const decoded = jwt.verify(token, privateKey);
      return decoded;
    } catch (error) {
      console.error(error);
      return null;
    }
  }

module.exports ={
    generateToken,
    verifyToken 
}
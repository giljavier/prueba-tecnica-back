const bcrypt = require('bcrypt');
const saltRounds = 10;

const encrypt = async (password) =>{
    const hash =  await bcrypt.hashSync(password, saltRounds);
    return hash;
}

const compare = async (userPassword,passwordHash) =>{
    const hash =  await bcrypt.compare(userPassword, passwordHash)
    return hash;
}

module.exports = {
    encrypt,
    compare
}
const { WareHouseModel} = require("../../dominio/models/warehouse.model");

class WarehouseController {

    static async ListWarehouse(req, res, next) {
       let obj = await WareHouseModel.ListWarehouse();
        res.send(obj);
    }

    static async PostWarehouse(req,res,next){       
        let obj  =  await WareHouseModel.NewWarehouse(req.body);
        res.send(obj);
    }

    static async UpdateWarehouse(req, res, next){
        let { id } = req.params; 
        let obj = await WareHouseModel.UpdateWarehouse(req.body, id);
        res.send(obj);
    }
    
}

module.exports = { WarehouseController }
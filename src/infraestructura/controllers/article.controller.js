const { ArticlesModel} = require("../../dominio/models/article.model");

class ArticlesController {

    static async ListArticles(req, res, next) {
       let obj = await ArticlesModel.ListArticles();
        res.send(obj);
    }

    static async PostArticles(req,res,next){       
        let obj  =  await ArticlesModel.NewArticles(req.body);
        res.send(obj);
    }

    static async UpdateArticles(req, res, next){
        let { id } = req.params; 
        let obj = await ArticlesModel.UpdateArticles(req.body, id);
        res.send(obj);
    }
    
}

module.exports = { ArticlesController }
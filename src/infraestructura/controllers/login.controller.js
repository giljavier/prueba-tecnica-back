const { LoginModel } = require("../../dominio/models/login.model");
const { encrypt } = require("../utils/cryptPass");

class LoginController {

    static async sigIn(req, res) {
       try {
        const {email, password} = req.body;
        if (email === null || password === null){
            return res.status(500).json({message:'El correo o la contraseña estan vacia'})
        }
        const obj = await LoginModel.get(email,password);
        res.send(obj);
       } catch (error) {
        res.status(500).json(error);
       }
    }

    static async singUp(req,res){
        try {
       const newUser = {
            usuario:req.body.email,
            password: await encrypt(req.body.password)

        }
        let obj  =  await LoginModel.create(newUser);
        res.send(obj);
        } catch (error) {
            res.status(500).json(error);
        }
        
    }

}

module.exports = { LoginController }
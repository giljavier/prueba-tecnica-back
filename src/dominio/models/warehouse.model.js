const { QueryDBService } = require("../../aplicacion/database/query_db_mysql");
const query = new QueryDBService();

class WareHouseModel {
    static async ListWarehouse(){
        const fmt = `SELECT art.nombre as articulo ,emp.nombre as empresa FROM inventario as inv 
        JOIN articulos as art ON art.id = inv.articulo_id
        JOIN empresas as emp  ON emp.nit = inv.empresa_id`
        return await query.consultaSQL(fmt, {}).catch(err => {
            console.error(err);
        })
    }

    static async NewWarehouse(obj) {
        const fmt = `INSERT INTO inventario SET ?`;
        return await query.consultaSQL(fmt, obj).catch(err => {
            console.error(err);
        })
    }

}

module.exports = { WareHouseModel }
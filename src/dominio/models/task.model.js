const { QueryDBService } = require("../../aplicacion/database/query_db_mysql");
const query = new QueryDBService();

class TaskModel {
    static async ListTask(){
        const fmt = `SELECT * FROM empresas`;
        return await query.consultaSQL(fmt, {}).catch(err => {
            console.error(err);
        })
    }

    static async NewTask(obj) {
        const fmt = `INSERT INTO empresas SET ?`;
        return await query.consultaSQL(fmt, obj).catch(err => {
            console.error(err);
        })
    }

    static async UpdateTask(obj, id)  {
        const fmt = `UPDATE empresas SET ? WHERE id=${id}`;
        return await query.consultaSQL(fmt, obj).catch(err => {
            console.error(err);
        })
    }
}

module.exports = { TaskModel }
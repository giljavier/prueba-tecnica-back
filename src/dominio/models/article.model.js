const { QueryDBService } = require("../../aplicacion/database/query_db_mysql");
const query = new QueryDBService();

class ArticlesModel {
    static async ListArticles(){
        const fmt = `SELECT * FROM articulos`;
        return await query.consultaSQL(fmt, {}).catch(err => {
            console.error(err);
        })
    }

    static async NewArticles(obj) {
        const fmt = `INSERT INTO articulos SET ?`;
        return await query.consultaSQL(fmt, obj).catch(err => {
            console.error(err);
        })
    }

    static async UpdateArticles(obj, id)  {
        const fmt = `UPDATE articulos SET ? WHERE id=${id}`;
        return await query.consultaSQL(fmt, obj).catch(err => {
            console.error(err);
        })
    }
}

module.exports = { ArticlesModel }
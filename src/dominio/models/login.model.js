
const { compare } = require("../../infraestructura/utils/cryptPass");
const { generateToken } = require("../../infraestructura/utils/jwt");
const { QueryDBService } = require("../../aplicacion/database/query_db_mysql");
const query = new QueryDBService();
const mysql = require('mysql');



class LoginModel {

    static async get(email, passwordU) {
        const con = mysql.createConnection({
            host: "inventario.cvhhj6gqhj6m.us-east-1.rds.amazonaws.com",
            user: 'root',
            password: 'desarrollo',
            database: 'inventario',
            port: '3306',
            connectTimeout: 60000,
          });
          
        return new Promise((resolve, reject) => {
          con.connect((err) => {
            if (err) {
              reject(err);
              return;
            }
            
            con.query(`SELECT * FROM usuarios WHERE usuario='${email}'`, async (err, result) => {
              if (err) {
                reject(err);
                return;
              }
      
              if (!result || !result[0]) {
                reject(new Error('Usuario no encontrado'));
                return;
              }
      
              const validPassword = await compare(passwordU, result[0].password);
      
              if (validPassword) {
                const token = generateToken();
                resolve({
                  email: result[0].usuario,
                  rol: result[0].rol_id,
                  token: token
                });
              } else {
                reject(new Error('Contraseña incorrecta'));
              }
            });
          });
        });
      }
      
 

    static async create(obj) {
        const fmt = `INSERT INTO usuarios SET ?`;
        return await query.consultaSQL(fmt, obj).catch(err => {
            console.error(err);
        })
    }
}

module.exports = { LoginModel }
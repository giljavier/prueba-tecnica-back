const {verifyToken} = require("../../infraestructura/utils/jwt");
const moment = require('moment');

const authMiddleware = (req, res, next) => {
    const {authorization} = req.headers;
    const token = authorization ? authorization.split(" ")[1] :null;
    
    if (token === '' || !token){
        return res.status(401).send('No tiene permitido ingresar')
    }

    const validToken = verifyToken(token);
    if (!validToken){
        return res.status(401).send('El token esta modificado')
    }
    if (validToken.exp <= moment().unix()) {
        return res.status(401).send({ message: "El token ha expirado" });
    }

    return next();
}

module.exports =authMiddleware ;
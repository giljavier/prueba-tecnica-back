require('dotenv').config();
 const express = require('express');
 const bodyParser = require('body-parser');
 const cors = require("cors");

const app = express();
app.use(cors());
app.options('*', cors());
//Setting
app.set('port',process.env.PORT || 8080);
// Database
const { DBService } = require("./aplicacion/database/database_service");

new DBService();

//Midlewares
app.use(express.json());
app.use(bodyParser.json({limit: '50mb'}))
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));


//Rutes
app.use("/api",[
  require('./infraestructura/Routes/task'),
  require('./infraestructura/Routes/login'),
  require('./infraestructura/Routes/articles'),
  require('./infraestructura/Routes/warehouse')
]); 





function normalizePort(val) {
    var port = parseInt(val, 10);
  
    if (isNaN(port)) {
      // named pipe
      return val;
    }
  
    if (port >= 0) {
      // port number
      return port;
    }
  
    return false;
  }
  app.get('/',(req,res) => res.send('<h1>Api running</h1>'))
app.listen(normalizePort(process.env.PORT || "8080"),()=>{
    console.log('Server on port', app.get('port'), );
});